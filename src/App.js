import React, {useState, useEffect} from 'react';
import Formulario from './components/Formulario'
import ListadoImagenes from './components/ListadoImagenes'

function App() {
  
  // state de la app
  const [busqueda, guardarBusqueda] = useState('');
  const [imagenes, guardarImagenes] = useState([]);

  // para realizar el paginador
  const [paginaactual, guardarPaginaActual] = useState(1); // inicia el pagina 1
  const [totalpaginas, guardarTotalPaginas] = useState(1); // almenos tendremos una pagina

  useEffect(() => {
    
    const consultarApi = async () => {
     
      if (busqueda === '') return; // no retornar nada si esta vacio
      
      const imagenesPorPagina = 30; // verificar api si tiene parametro de paginador
      const key = '15951332-c900b3fd9463731479751e90c';
      const url = `https://pixabay.com/api/?key=${key}&q=${busqueda}&per_page=${imagenesPorPagina}&page=${paginaactual}`;

      const respuesta = await fetch(url);
      const resultado = await respuesta.json();

      guardarImagenes(resultado.hits);

      // guardar total paginas ( para el paginador)
      const calcularTotalPaginas = Math.ceil(resultado.totalHits / imagenesPorPagina); // para ver cuantos resultados tendremos
      guardarTotalPaginas(calcularTotalPaginas); 

      // cuando le das siguiente .. ponerse al inicio de las imagenes
      const jumbotron = document.querySelector('.jumbotron');
      jumbotron.scrollIntoView({behavior: 'smooth'}); // scroll que llevara hacia arriba

    }

    consultarApi();

  }, [busqueda, paginaactual]);

  // definir la pagina anterior
  const paginaAnterior = () => {
    const nuevaPaginaActual = paginaactual - 1;

    if (nuevaPaginaActual === 0) return; // para que no haya -1, -2 ..etc

    guardarPaginaActual(nuevaPaginaActual);
  }

  // definir la pagina siguiente
  const paginaSiguiente = () => {
    const nuevaPaginaActual = paginaactual + 1;

    if (nuevaPaginaActual > totalpaginas) return; // para que no haya mayor al nuero de paginas existentes

    guardarPaginaActual(nuevaPaginaActual);
  }

  return (
    <div className="container">
      <div className="jumbotron">
        <p className="lead text-center">Buscador de Imagenes</p>
        <Formulario
          guardarBusqueda={guardarBusqueda}
        />
      </div>

      <div className="row justify-content-center">
        <ListadoImagenes
          imagenes={imagenes}
        />
        
        {(paginaactual === 1)
          ? null
          : (
            <button
              type="button"
              className="bbtn btn-info mr-1"
              onClick={paginaAnterior}
            >&laquo; Anterior</button>
          )
        }

        {(paginaactual === totalpaginas)
          ? null
          : (
            <button
              type="button"
              className="bbtn btn-info"
              onClick={paginaSiguiente}
            >Siguiente &raquo;</button>
          )
         }

      </div>
    </div>
  );
}

export default App;

//  uso de tema pa bootstrap para el proyecto "   https://bootswatch.com/   "